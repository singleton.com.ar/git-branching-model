#!/usr/bin/env bash
DIR__=""
if [ ! -d ../Hotfix ]; then
  DIR__="/opt/git-branching-model/Scripts/HotFix/"
fi
source $DIR__../Common/variables.sh
source $DIR__../Version/bump-version.sh -p

git $FLAGS checkout -b hotfix-"$(cat "$__PATH"/VERSION)" master