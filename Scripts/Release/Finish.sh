#!/usr/bin/env bash
DIR__=""
if [ ! -d ../Feature ]; then
  DIR__="/opt/git-branching-model/Scripts/Feature/"
fi
source $DIR__../Common/variables.sh
if [ -f $__PATH/VERSION ]; then
  VERSION=`cat $__PATH/VERSION`  
  PUSHING_MSG="${NOTICE_FLAG} Pushing version "$VERSION" to the ${B_WHITE}origin${B_CYAN}..."  
  if [ "$(git $FLAGS branch | grep \* | cut -d '-' -f2)" -ne "$VERSION" ]; then
    echo -e "${B_RED}ERROR: There is not a version branch for the current version.${RESET}"
    exit 1
  fi
  if [ $(git $FLAGS tag --list | grep -q $VERSION) ]; then #If the version is already tagged.
    echo -e "${B_RED}ERROR: Version number has been previously created.${RESET}"
    exit 1
  else
    git $FLAGS add CHANGELOG.md VERSION
    git $FLAGS commit -m "Bump version to ${VERSION}."
    git $FLAGS tag -a -m "Tag version ${VERSION}." "v$VERSION"
    if [ $(git remote -v | wc -l) != 0 ] ; then
      git $FLAGS push origin --tags
    fi
    git $FLAGS checkout master
    git $FLAGS merge --no-ff release-$VERSION -m "Merge branch release-$VERSION into master"
    git $FLAGS checkout develop
    git $FLAGS merge --no-ff release-$VERSION -m "Merge branch release-$VERSION into develop"
    git $FLAGS branch -d release-$VERSION
    if [ $(git remote -v | wc -l) != 0 ] ; then
      echo -e "$PUSHING_MSG"
      git $FLAGS push origin
    fi
  fi
else
  echo -e "${B_RED}ERROR: Version file not found. Execute new_release.sh before.${RESET}"
  exit 1
fi