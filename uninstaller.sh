#!/usr/bin/env bash
source Scripts/Common/variables.sh
LAST_VERSION="$(cat VERSION)"
echo -e "${B_GREEN}Singleton Git Branching Model. v$LAST_VERSION ${B_RED} UNINSTALLER${RESET}"
echo -ne "${QUESTION_FLAG} ${CYAN}Do you want to ${B_RED}UNINSTALL ${B_CYAN}Singleton Git Branching Model${CYAN}? [${WHITE}y${CYAN}]: ${RESET}"
read RESPONSE
if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "y" ]; then
  sudo rm -r /opt/git-branching-model
  sudo rm -r /usr/local/bin/branching-model
fi